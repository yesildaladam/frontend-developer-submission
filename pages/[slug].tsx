import Portfolio from "components/assets/Assets";
import Index from "pages";
import { getCollectionDetails } from "services/apiCalls";
import Web3 from "web3";
import { Alchemy, Network } from "alchemy-sdk";
const web3 = new Web3();


const config = {
  apiKey: "3G5FtF7xZHU8mt07wq2LjnIpqBZnS89Z",
  network: Network.ETH_MAINNET,
};
const alchemy = new Alchemy(config);

type PageMetadataProps = {
  pageType: "portfolio";
  collectionData:  []
  assetData:  any[]

};

export function isValidEthereumAddress(address: string): boolean {
  return web3.utils.isAddress(address);
}

const page = ({ pageType,collectionData: collectionData,assetData: assetData }: PageMetadataProps) => {
  if (pageType == "portfolio") {
    return <Portfolio collectionData={collectionData} assetData={assetData} />;
  }
  return (
    <>
      <main>
        <Index />
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  let found = false;

  let props: PageMetadataProps = {
    pageType: "portfolio",
    collectionData: [],
    assetData: []
  };

  const userAddress = context.params.slug

  const isValid = isValidEthereumAddress(userAddress);
  if (isValid == false) {
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
  const Collections = await getCollectionDetails(
    userAddress
  );

  const Assets = await alchemy.nft.getNftsForOwner(userAddress);

  const nftData = []
  
  for (let i = 0; i < Assets.ownedNfts.length; i++) {
    nftData.push(JSON.parse(JSON.stringify(Assets.ownedNfts[i])))
  }

  
  if (typeof Collections === "object" && typeof Assets === "object" ) {
    found = true;
    props.collectionData = Collections;
    props.assetData = nftData;
  }

  if (found) {
    return {
      props: props,
    };
  } else {
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
}

export default page;
