import Portfolio from "components/collection/Collection";
import Index from "pages";
import { getCollectionDetails } from "services/apiCalls";
import Web3 from "web3";
const web3 = new Web3();

type PageMetadataProps = {
  pageType: "portfolio";
  collectionData:  []
};

export function isValidEthereumAddress(address: string): boolean {
  return web3.utils.isAddress(address);
}

const page = ({ pageType,collectionData: collectionData}: PageMetadataProps) => {
  if (pageType == "portfolio") {
    return <Portfolio collectionData={collectionData} />;
  }
  return (
    <>
      <main>
        <Index />
      </main>
    </>
  );
};

export async function getServerSideProps(context) {
  let found = false;

  let props: PageMetadataProps = {
    pageType: "portfolio",
    collectionData: [],
  };

  const userAddress = context.params.slug

  const isValid = isValidEthereumAddress(userAddress);
  if (isValid == false) {
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
  const Collections = await getCollectionDetails(
    userAddress
  );

  
  if (typeof Collections === "object") {
    found = true;
    props.collectionData = Collections;
  }

  if (found) {
    return {
      props: props,
    };
  } else {
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
}

export default page;
