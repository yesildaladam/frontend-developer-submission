import { useState } from "react";


export default function Page404() {

  const [texts] = useState({
    pageTitle: 'Address not found',
    pageDescription:'Please be sure you have given the correct address',
  });

  return (
    <>
        <div className="container">
          <h2>{texts.pageTitle}</h2>
          <p dangerouslySetInnerHTML={{__html: texts.pageDescription}} >
          </p>
        </div>
      <style jsx>{`
        .container {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-direction: column;
          position: relative;
          background: black;
          height: 100%;
          height: 101vh !important;
        }

        .container h2 {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          line-height: 135%;
          margin-bottom: 2.777vw;
          font-size: min(35px, 6vw);
          line-height: 105%;
          z-index: 1;
          width: 60%;
          text-align: center;
          color: white;
        }

        .container p {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          color: #234d63;
          font-size: min(20px, 3.6vw);
          z-index: 1;
          padding-left: 10vw;
          padding-right: 10vw;
          text-align: center;
          width: 80%;
          color: white;
        }

        .container p a {
          color: var(--title-color);

        }


        .container img {
          width: 100%;
          position: relative;
          height: 100%;
          object-fit: cover;
        }

      `}</style>
    </>
  );
}
