import Home from "components/home/Home";

export default function Index() {
  return (
    <>
      <main>
        <div id="index">
          <section id="home">
            <Home />
          </section>
        </div>
      </main>
      <style jsx>{`
        .index {
          background: #000000;
          height: 100%;
        }

      `}</style>
    </>
  );
}
