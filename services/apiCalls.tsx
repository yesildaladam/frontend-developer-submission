
const openseaCollectionsBaseURL = "https://api.opensea.io/api/v1/collections"
const openseaUsersBaseURL = "https://api.opensea.io/api/v1/assets"


//https://api.opensea.io/api/v1/collections?asset_owner=0x0000000000000000000000000000000000000000&offset=0&limit=100
export async function getCollectionDetails(address: string){
  const url = `${openseaCollectionsBaseURL}/?asset_owner=${address}&offset=0&limit=100`;
  const response = await fetch(url);
  const json = await response.json();
  return json;
}
