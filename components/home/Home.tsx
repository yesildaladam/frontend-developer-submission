import ButtonBox from "components/shared/ButtonBox";
import { useState } from "react";

/**
 *
 * Home of the app, first page you see.
 *
 * @param {String} title - Title of the component.
 * @param {String} subtitle - Subtitle of the component.
 *
 */

export default function Home() {
  const [texts] = useState({
    title: "Check out your NFTs in an instant!",
    subtitle: "Get all the NFTs and collection data from any address",
  });
  return (
    <>
      <div className={`intro`}>
        <div className="content">
          <h1>{texts.title}</h1>
          <p dangerouslySetInnerHTML={{ __html: texts.subtitle }}></p>
        </div>
        <div className={`preview-container`}>
          <div className="preview-inner">
            <ButtonBox />
          </div>
        </div>
      </div>
      <style jsx>{`
        .intro {
          background-color: #000000;
          padding: 2.431vw 3.571vw;
          display:flex;
          flex-direction: column;
          grid-gap: 2vw
          height: 100%;
          min-height: 100vh;
          justify-content: center;
          align-items: center;
        }

        .intro .content {
          display: flex;
          flex-direction: column;
        }

        .intro .content h1 {
          font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
            Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: clamp(5.0264vw, 5.0264vw, 154.5px);
          line-height: 115%;
          color: color(display-p3 0.549 0.929 0.996);
          margin-bottom: 1.435vw;
          animation: fadeIn 1.2s cubic-bezier(0.65, 0, 0.35, 1) forwards;
          color: white;
          text-align: center;

        }

        .intro .content p {
          font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
            Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: clamp(1.521164vw, 1.521164vw, 46.76px);
          line-height: 128%;
          margin-bottom: 2.248vw;
          animation: fadeIn 1.2s cubic-bezier(0.65, 0, 0.35, 1) forwards;
          color: white;
          text-align: center;
        }

        .preview-container {
          display: flex;
          justify-content: center;
        }

        @media screen and (max-width: 992px) {
          .intro {
            padding: 23px 22px;
            height: 101vh !important;
            border-radius: 0 !important;
            flex-direction: column;
          }
          .intro .content {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: stretch;
            margin-left: auto;
            margin-right: auto;
            padding-top: 64px;
          }

          .intro .content h1 {
            font-size: 6vw;
            width: 100%;
            line-height: 120%;
            font-weight: 500;
            text-align: center;
            text-shadow: 0 0 24px #1e4b6b88;
            margin-bottom: 16px;
          }

          .intro .content p {
            font-size: 3.5vw;
            line-height: 128%;
            width: 100%;
            padding: 0 15px;
            font-weight: 300;
            text-align: center;
          }

        }

        @media screen AND (max-width: 526px) {
          .intro {
            padding-left: 4vw;
            padding-right: 4vw;
            flex-direction: column;
          }

          .intro .content h1 {
            font-size: 12vw;
            width: 100%;
            line-height: 120%;
            font-weight: 500;
            text-align: center;
            text-shadow: 0 0 24px #1e4b6b88;
            margin-bottom: 16px;
          }

          .intro .content p {
            font-size: 4vw;
            line-height: 128%;
            width: 100%;
            padding: 0 15px;
            font-weight: 300;
            text-align: center;
          }
        }
      `}</style>
      <style jsx global>{`
        .intro .content .action span strong {
          font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
            Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: clamp(1.25661vw, 1.25661vw, 38.63px);
          line-height: 128%;
          color: #fdbab2;
        }

        @media screen and (max-width: 992px) {
          .intro .content .action span strong {
            font-size: 15.6443px;
            line-height: 128%;
          }
        }

        @media screen AND (min-width: 526px) AND (max-width: 992px) {
          .intro .content .action span strong {
            font-size: 20px;
          }
        }

        @media screen AND (min-width: 1921px) {
          .intro .content .action span strong {
            font-size: 1vw;
          }
        }
      `}</style>
    </>
  );
}
