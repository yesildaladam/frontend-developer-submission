import { useState } from "react";

/**
 *
 * Button component, this component is used at the very first page and redirects to wallet page.
 *
 * @param {String} title - If i18n used, title can be adjustable
 * @param {String} collectionButtonText - If i18n used, button text can be adjustable
 * @param {String} assetButtonText - If i18n used, button text can be adjustable
 *
 */

export default function ButtonBox() {
  const [address, setAddress] = useState("");

  const handleAddressChange = (givenAddress: string) => {
    setAddress(givenAddress);
  };
  const [texts] = useState({
    title: "Enter an ETH address or ENS",
    collectionButtonText: "Collection",
    assetButtonText: "NFTs",

  });
  return (
    <>
      <div className={`container`}>
        <span>{texts.title}</span>
        <input
          type="text"
          onChange={(e) => {
            handleAddressChange(e.target.value);
          }}
        />
        <div className="button-row">
          <a href={"collection/" +address}>
            <button style={{backgroundColor: "#00F9FF"}} >{texts.collectionButtonText}</button>
          </a>
          <a href={address}>
            <button style={{backgroundColor: "#F65BE1"}} >{texts.assetButtonText}</button>
          </a>
        </div>
      </div>
      <style jsx>
        {`
          .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: 100%;
            border-radius: 1.5rem;
            padding: 2rem;
            background: #1b1b1d;
          }

          .container span {
            font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
              Helvetica, Arial, sans-serif, "Apple Color Emoji",
              "Segoe UI Emoji";
            font-style: normal;
            font-weight: 400;
            font-size: clamp(1.521164vw, 1.521164vw, 46.76px);
            line-height: 128%;
            width: 100%;
            margin-bottom: 2.248vw;
            color: white;
          }

          .container input {
            margin-bottom: 2.248vw;
            width: 100%;
            padding: 1vw 2vw;
            background: none;
            border: 1px solid white;
            border-radius: 2vw;
            transition: border-color 0.3s;
            color: white;
          }

          .container input:focus {
            outline: none;
          }

          .container a button {
            padding: 0.5vw 2vw;
            border: transparent;
            border-radius: 2vw;
            color: black;
            margin: 5px;
          }

          .container button:hover {
            background-color: #f99ced;
          }

          @media screen AND (max-width: 950px) {
            .container {
              margin-top: 2rem;
            }
            .container span {
              font-size: 3vw;
            }

            .container button {
              padding: 2vw 2vw;
              font-size: 2vw;
              margin-top: 2vw;
            }
          }

          @media screen AND (max-width: 526px) {
            .container {
              margin-top: 2rem;
            }
            .container span {
              font-size: 5vw;
            }

            .container button {
              padding: 3vw 3vw;
              font-size: 3vw;
              margin-top: 2vw;
            }
          }
        `}
      </style>
    </>
  );
}
