import React, { useState } from "react";
import Modal from "@mui/material/Modal";
import AssetsDetailedItem from "./DetailedItem/AssetsDetailedItem";

type AssetsGridItemProps = {
  itemData: any[];
};

/**
 *
 * Item of a grid, image, name, volume and owned asset can be seen.
 *
 * @param {any[]} itemData - Collection data.
 *
 */

function AssetsGridItem(props: AssetsGridItemProps) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [nft] = useState({
    image:
      props.itemData["media"].length == 0
        ? props.itemData["contract"]["openSea"]["imageUrl"]
        : props.itemData["media"][0]["gateway"],
    mediaType:
      props.itemData["media"].length == 0
        ? "mp4"
        : props.itemData["media"][0]["format"],
    name: props.itemData["title"],
    tokenid: props.itemData["tokenId"],
    collectionName: props.itemData["contract"]["openSea"]["collectionName"],
    collectionImage: props.itemData["contract"]["openSea"]["imageUrl"],
    floor: props.itemData["contract"]["openSea"]["floorPrice"],
  });

  return (
    <>
      <a onClick={handleOpen}>
        <div className="container">
          <img
            src={nft.mediaType == "mp4" ? nft.collectionImage : nft.image}
            alt=""
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src =
                "https://openseauserdata.com/files/49499cddba4d6d445125043f2592eb7b.png";
            }}
          />

          <div className="cta-container">
            <h2
              dangerouslySetInnerHTML={{
                __html: nft.name,
              }}
            ></h2>
            <p>{nft.collectionName}</p>
            <span>
              {nft.floor == undefined
                ? "Spam Collection"
                : "Floor: " + nft.floor + " ETH"}
            </span>
          </div>
        </div>
      </a>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <AssetsDetailedItem itemData={props.itemData} />
      </Modal>
      <style jsx>{`
        .container {
          display: flex;
          justify-content: space-between;
          flex-direction: column;
          box-sizing: border-box;
          border-radius: 16px;
          background-position: top 4vw left 4vw;
          background-repeat: no-repeat;
          background-size: 5%;
          margin-top: 16px;
          background: #1b1b1d;
        }

        .container:hover {
          background: #141415;
        }

        h2 {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: 1.5vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 20px;
          padding-left: 20px;
        }
        p {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1.2vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 20px;
          padding-left: 20px;
        }

        span {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 20px;
          padding-left: 20px;
          padding-bottom: 20px;
        }
        .text {
          color: var(--theme-color);
          font-size: 0.9vw;
          line-height: 0vw;
          margin-bottom: 1vw;
        }

        .text strong {
          font-weight: 400;
        }

        .cta-container {
          display: flex;
          flex-direction: column;
        }

        .btn {
          justify-content: center;
          font-size: 0.9vw;
          padding-left: 12px;
          padding-right: 12px;
          margin-top: 3vh;
          align-self: flex-start;
          height: 2.2vw;
          background-color: var(--title-color);
          color: white;
        }

        .container img {
          border-top-left-radius: 16px;
          border-top-right-radius: 16px;
          aspect-ratio: 3/2;
          object-fit: cover;
          width: 100%;
        }

        .button-container {
          padding-top: 30px;
          display: flex;
        }

        @media screen AND (max-width: 950px) {
          .btn {
            justify-content: center;
            font-size: 0.9vw;
            align-self: flex-start;
            background-color: var(--title-color);
            color: white;
            margin-top: 2vh;
            min-height: 2vh;
          }
        }

        @media screen AND (max-width: 526px) {
          .container {
            flex-direction: column;
            text-align: center;
          }
          h2 {
            font-size: 4vw;
            color: white;
            padding-top: 20px;
            padding-left: 0px;
          }
          p {
            font-size: 4vw;
            padding-top: 20px;
            padding-left: 0px;
          }

          span {
            font-size: 3vw;
            padding-top: 20px;
            padding-left: 0px;
            padding-bottom: 0px;
          }

          .cta-container {
            flex: 1;
            padding-left: 24px;
            padding-right: 24px;
            padding-bottom: 24px;
          }
        }
      `}</style>
    </>
  );
}

export default AssetsGridItem;
