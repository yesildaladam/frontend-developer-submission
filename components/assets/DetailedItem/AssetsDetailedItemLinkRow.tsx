import React, { useState } from "react";

type AssetsDetailedItemLinkRowProps = {
  discord: string;
  website: string;
  twitter: string;
};

/**
 *
 * The link row for the collection.
 *
 * @param {string} discord - Discord link for the collection.
 * @param {string} website - Website link for the collection.
 * @param {string} twitter - Twitter link for the collection.
 *
 */

function AssetsDetailedItemLinkRow(props: AssetsDetailedItemLinkRowProps) {
  const [links] = useState({
    discord: props.discord,
    website: props.website,
    twitter: props.twitter
  });

  return (
    <>
      <div className="link-row">
        <span>
          <a href={links.website}> WEBSITE </a>
        </span>
        <span>|</span>
        <span>
          <a href={"https://twitter.com/" +links.twitter}> TWITTER </a>
        </span>
        <span>|</span>
        <span>
          <a href={links.discord}> DISCORD </a>
        </span>
      </div>
      <style jsx>{`

      .link-row{
        padding-top: 10px;
        padding-left: 10px;
      }

       .link-row span a{
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: #6A7280;
          text-align: start;
        }

        .link-row span a:hover{
          color: white;
        }
        .link-row span{
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: #6A7280;
          text-align: start;
          padding-left: 5px;
          padding-right: 5px;

        }

        @media screen AND (max-width: 1200px) {
          p {
            font-size: 1.2vw;
          }
        }

        @media screen AND (max-width: 950px) {
          .link-row{
            padding-top: 0px;
            padding-left: 0px;
          }
        }

        @media screen AND (max-width: 526px) {
          p {
            font-size: 2.5vw;
          }
          .link-row span{
            font-size: 3vw;
            padding-left: 0px;

          }
          .link-row span a{
            font-size: 3vw;
          }
        }
      `}</style>
    </>
  );
}

export default AssetsDetailedItemLinkRow;
