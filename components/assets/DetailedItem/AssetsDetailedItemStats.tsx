import React, { useState } from "react";

type AssetsDetailedItemStatsProps = {
  floor: string;
  totalSupply: string;
};

/**
 *
 * The detailed stats of the asset.
 *
 * @param {string} floor - Floor price of the asset
 * @param {string} totalSupply - Total supply of the asset
 *
 */

function AssetsDetailedItemStats(props: AssetsDetailedItemStatsProps) {
  const [nft] = useState({
    floor: props.floor,
    totalSupply: props.totalSupply
  });

  return (
    <>
      <div className="details">
        <div className="detail-block">
          <p>{"Floor"}</p>
          <p>{nft.floor == undefined ? "Spam Collection" :nft.floor + " ETH"}</p>
        </div>
        <div className="detail-block">
          <p>{"Total Supply"}</p>
          <p>{nft.totalSupply}</p>
        </div>
      </div>
      <style jsx>{`
        .details {
          position: relative;
          box-sizing: border-box;
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-gap: 2vw;
          padding: 20px;
        }

        p {
          font-size: 0.8vw;
          color: white;
        }

        .details .detail-block {
          display: flex;
          flex-direction: column;
          row-gap: 10px;
        }

        @media screen AND (max-width: 1200px) {
          p {
            font-size: 1.2vw;
          }
        }

        @media screen AND (max-width: 950px) {
          p {
            font-size: 1.5vw;
          }
          .details {
            position: relative;
            box-sizing: border-box;
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-gap: 2vw;
            padding-left: 0px;
            padding-top: 10px;
          }
        }

        @media screen AND (max-width: 526px) {
          p {
            font-size: 3.5vw;
          }
          .details {
            position: relative;
            box-sizing: border-box;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 2vw;
          }
        }
      `}</style>
    </>
  );
}

export default AssetsDetailedItemStats;
