import React, { useEffect, useState } from "react";
import AssetsDetailedItemLinkRow from "./AssetsDetailedItemLinkRow";
import AssetsDetailedItemStats from "./AssetsDetailedItemStats";
type AssetsDetailedItemProps = {
  itemData: any[];
};

/**
 *
 * The detailed item modal.
 *
 * @param {any[]} itemData - Asset data of the detailed item.
 *
 */


function AssetsDetailedItem(props: AssetsDetailedItemProps) {
  const [nft] = useState({
    collectionImage: props.itemData["contract"]["openSea"]["imageUrl"],
    image:
      props.itemData["media"].length == 0
        ? props.itemData["contract"]["openSea"]["imageUrl"]
        : props.itemData["media"][0]["gateway"],
    name: props.itemData["title"],
    description: props.itemData["contract"]["openSea"]["description"],
    totalSupply: props.itemData["contract"]["totalSupply"],
    contractAddress: props.itemData["contract"]["address"],
    openseaName: props.itemData["contract"]["openSea"]["collectionName"],
    tokenid: props.itemData["tokenId"],
    collectionName: props.itemData["contract"]["openSea"]["collectionName"],
    externalUrl: props.itemData["contract"]["openSea"]["externalUrl"] || "",
    twitterUsername:
      props.itemData["contract"]["openSea"]["twitterUsername"] || "",
    discordUrl: props.itemData["contract"]["openSea"]["discordUrl"] || "",
    floor: props.itemData["contract"]["openSea"]["floorPrice"],
    tokenType: props.itemData["tokenType"],
  });


  return (
    <>
      <div className="container">
        <div className="image-row">
          <img src={nft.collectionImage} alt="" />
          <h2
            dangerouslySetInnerHTML={{
              __html: nft.name,
            }}
          ></h2>
        </div>
        <AssetsDetailedItemLinkRow
          discord={nft.discordUrl}
          website={nft.externalUrl}
          twitter={nft.twitterUsername}
        />
        <div className="cta-container">
          <p
            dangerouslySetInnerHTML={{
              __html: nft.description,
            }}
          ></p>
          <a
            href={
              "https://etherscan.io/token/" +
              nft.contractAddress +
              "#readContract"
            }
          >
            <span
              dangerouslySetInnerHTML={{
                __html: "Contract: " + nft.contractAddress,
              }}
            ></span>
          </a>
          <span
            dangerouslySetInnerHTML={{
              __html: "Token Type: " + nft.tokenType,
            }}
          ></span>
          <a
            href={
              "https://opensea.io/assets/ethereum/" +
              nft.contractAddress +
              "/" +
              nft.tokenid
            }
          >
            <button>Buy on Opensea</button>
          </a>
          <AssetsDetailedItemStats floor={nft.floor} totalSupply={nft.totalSupply} />
        </div>
      </div>
      <style jsx>{`
        .container {
          display: flex;
          flex-direction: column;
          box-sizing: border-box;
          border-radius: 16px;
          background-position: top 4vw left 4vw;
          background-repeat: no-repeat;
          background-size: 5%;
          margin-top: 16px;
          background: #1b1b1d;
          width: 40%;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          padding: 20px;
        }

        .container .image-row {
          display: flex;
          flex-direction: row;
          align-items: center;
        }

        h2 {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: 2vw;
          line-height: 1.6vw;
          color: white;
        }

        button {
          background-color: #2081e2;
          padding: 0.5vw 2vw;
          border: transparent;
          border-radius: 2vw;
          margin: 20px;
          color: white;
        }

        button:hover {
          background-color: #8fc0f0;
        }

        p {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 20px;
          padding-left: 20px;
          padding-bottom: 20px;
        }

        span {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-left: 20px;
        }
        .text {
          color: var(--theme-color);
          font-size: 0.9vw;
          line-height: 0vw;
          margin-bottom: 1vw;
        }

        .text strong {
          font-weight: 400;
        }

        .cta-container {
          display: flex;
          flex-direction: column;
        }

        .cta-container link-row span {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 300;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 40px;
          padding-left: 20px;
        }

        .btn {
          justify-content: center;
          font-size: 0.9vw;
          padding-left: 12px;
          padding-right: 12px;
          margin-top: 3vh;
          align-self: flex-start;
          height: 2.2vw;
          background-color: var(--title-color);
          color: white;
        }

        .container img {
          border-radius: 16px;
          object-fit: cover;
          width: 20%;
          margin: 20px;
        }

        .button-container {
          padding-top: 30px;
          display: flex;
        }

        @media screen AND (max-width: 950px) {

          h2 {
            font-size: 2vw;
            line-height: 120%;
            color: white;
            padding-left: 0px;
          }
          p {
            font-size: 1.2vw;
            padding-top: 20px;
            padding-left: 0px;
          }
  
          span {
            font-size: 1.2vw;
            padding-top: 5px;
            padding-left: 0px;
            padding-bottom: 0px;
          }

          .container img {
            border-radius: 16px;
            object-fit: cover;
            width: 20%;
            margin-left: 0px;
          }

          button {
            background-color: #2081e2;
            padding: 0.5vw 2vw;
            border: transparent;
            border-radius: 2vw;
            margin-left: 0px;
            color: white;
          }

        }

        @media screen AND (max-width: 526px) {
          .container {
            width: 75%;
          }

          h2 {
            font-size: 5vw;
            line-height: 120%;
            color: white;
            padding-left: 0px;
          }
          p {
            font-size: 3vw;
            line-height: 130%;
            padding-top: 20px;
            padding-left: 0px;
          }
  
          span {
            font-size: 2.5vw;
            padding-top: 5px;
            padding-left: 0px;
            padding-bottom: 0px;
          }

          .container img {
            border-radius: 16px;
            object-fit: cover;
            width: 30%;
            margin-left: 0px;
          }

          button {
            background-color: #2081e2;
            padding: 2vw 2vw;
            border: transparent;
            border-radius: 2vw;
            margin-left: 0px;
            color: white;
          }
        }
      `}</style>
    </>
  );
}

export default AssetsDetailedItem;
