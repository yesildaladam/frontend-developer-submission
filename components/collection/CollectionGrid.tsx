import { useState } from "react";
import CollectionGridItem from "./CollectionGridItem";


type CollectionGridProps = {
  data: any[];
};

/**
 *
 * Collection grid component, 
 *
 * @param {any[]} data - Collection data.
 *
 */


export default function CollectionGrid(props: CollectionGridProps) {
  const [nfts] = useState({
    nfts: props.data,
  });
  return (
    <>
      <section id="section">
        <div className="content">
          {nfts.nfts.map((item, index) => {
            return (
              <CollectionGridItem
              itemData={item}
              />
            );
          })}
        </div>
      </section>
      <style jsx>{`
        section {
          position: relative;
          display: flex;
          flex-direction: row;
          justify-content: center;
          background: black;
        }

        section .content {
          position: relative;
          box-sizing: border-box;
          padding: 2.431vw 3.571vw;
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-gap: 2vw;
        }

        section .content h2 {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: 3.10846vw;
          line-height: 100%;
          color: #234d63;
          margin-right: 3.306vw;
          animation-delay: 0s;
        }

        @media screen AND (max-width: 526px) {
          section .content {
            position: relative;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
          }
        }
      `}</style>
    </>
  );
}
