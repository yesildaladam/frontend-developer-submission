import React, { useState } from "react";

type CollectionGridItemStatsProps = {
  ownedAsset: string;
  floor: string;
  volume: number;
};

/**
 *
 * The owned asset and volume stats at the bottom of the grid item.
 *
 * @param {String} ownedAsset - How many asset the user owns from the collection.
 * @param {String} volume - The volume of the collection.
 *
 */

function CollectionGridItemStats(props: CollectionGridItemStatsProps) {
  const [nft] = useState({
    ownedAsset: props.ownedAsset,
    volume: props.volume.toFixed(2)
  });


  return (
    <>
      <div className="details">
        <div className="detail-block">
          <p>{"Volume"}</p>
          <p>{ nft.volume + " ETH"}</p>
        </div>
        <div className="detail-block">
          <p>{"Owned"}</p>
          <p>{nft.ownedAsset}</p>
        </div>
      </div>
      <style jsx>{`
        .details {
          position: relative;
          box-sizing: border-box;
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-gap: 2vw;
          padding: 20px;
        }

        p {
          font-size: 0.8vw;
          color: white;
        }

        .details .detail-block {
          display: flex;
          flex-direction: column;
          row-gap: 10px;
        }

        @media screen AND (max-width: 1200px) {
          p {
            font-size: 1.2vw;
          }
        }

        @media screen AND (max-width: 950px) {
          p {
            font-size: 1.5vw;
          }
        }

        @media screen AND (max-width: 526px) {
          .details {
            padding: 0px;
            padding-top: 20px;
            grid-template-columns: repeat(2, 1fr);

          }
          .details .detail-block {
            display: flex;
            flex-direction: column;
            row-gap: 10px;
            align-items: start;
          }
          p {
            font-size: 2.5vw;
          }
        }
      `}</style>
    </>
  );
}

export default CollectionGridItemStats;
