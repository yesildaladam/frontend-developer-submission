import React, { useState } from "react";
import CollectionDetailedItemStats from "./CollectionDetailedItemStats";

type CollectionDetailedItemProps = {
  collectionData: any[];
};

/**
 *
 * The detailed item modal.
 *
 * @param {any[]} collectionData - Collection data of the collection for the detailed item.
 *
 */


function CollectionDetailedItem(props: CollectionDetailedItemProps) {
  const [nft] = useState({
    image: props.collectionData["image_url"],
    name: props.collectionData["name"],
    description: props.collectionData["description"],
    contractAddress: props.collectionData["primary_asset_contracts"]["0"]["address"],
    openseaName: props.collectionData["slug"],
    ownedAsset: props.collectionData["owned_asset_count"],
    floor: props.collectionData["stats"]["floor_price"],
    volume: props.collectionData["stats"]["total_volume"],
    totalSupply: props.collectionData["stats"]["total_supply"],
    ownerNumber: props.collectionData["stats"]["num_owners"],
    totalSales: props.collectionData["stats"]["total_sales"],
  });

  return (
    <>
      <div className="container">
        <div className="image-row">
          <img src={nft.image} alt="" />
          <h2
            dangerouslySetInnerHTML={{
              __html: nft.name,
            }}
          ></h2>
        </div>{" "}
        <div className="cta-container">
          <p
            dangerouslySetInnerHTML={{
              __html: nft.description,
            }}
          ></p>
          <a
            href={
              "https://etherscan.io/token/" +
              nft.contractAddress +
              "#readContract"
            }
          >
            <span
              dangerouslySetInnerHTML={{
                __html: "Contract: " + nft.contractAddress,
              }}
            ></span>
          </a>
          <a href={"https://opensea.io/collection/" + nft.openseaName}>
            <button>Buy on Opensea</button>
          </a>
          <div className="details-container">
            <CollectionDetailedItemStats
              ownedAsset={nft.ownedAsset}
              volume={nft.volume}
              totalSupply={nft.totalSupply}
              ownerNumber={nft.ownerNumber}
              totalSales={nft.totalSales}
            />
          </div>
        </div>
      </div>
      <style jsx>{`
        .container {
          display: flex;
          flex-direction: column;
          box-sizing: border-box;
          border-radius: 16px;
          background-position: top 4vw left 4vw;
          background-repeat: no-repeat;
          background-size: 5%;
          margin-top: 16px;
          background: #1b1b1d;
          width: 40%;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          padding: 20px;
        }

        .container .image-row {
          display: flex;
          flex-direction: row;
          align-items: center;
        }

        h2 {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: 2vw;
          line-height: 1.6vw;
          color: white;
        }

        button {
          background-color: #2081e2;
          padding: 0.5vw 2vw;
          border: transparent;
          border-radius: 2vw;
          margin: 20px;
          color: white;
        }

        button:hover {
          background-color: #8fc0f0;
        }

        p {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 20px;
          padding-left: 20px;
          padding-bottom: 20px;
        }

        span {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-left: 20px;
        }
        .text {
          color: var(--theme-color);
          font-size: 0.9vw;
          line-height: 0vw;
          margin-bottom: 1vw;
        }

        .text strong {
          font-weight: 400;
        }

        .cta-container {
          display: flex;
          flex-direction: column;
        }

        .cta-container link-row span {
          font-family: TWK Everett, -apple-system, BlinkMacSystemFont,
            "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji",
            "Segoe UI Emoji";
          font-style: normal;
          font-weight: 300;
          font-size: 1vw;
          line-height: 1.6vw;
          color: white;
          text-align: start;
          padding-top: 40px;
          padding-left: 20px;
        }

        .btn {
          justify-content: center;
          font-size: 0.9vw;
          padding-left: 12px;
          padding-right: 12px;
          margin-top: 3vh;
          align-self: flex-start;
          height: 2.2vw;
          background-color: var(--title-color);
          color: white;
        }

        .container img {
          border-radius: 16px;
          object-fit: cover;
          width: 20%;
          margin: 20px;
        }

        .button-container {
          padding-top: 30px;
          display: flex;
        }

        @media screen AND (max-width: 950px) {
          h2 {
            font-size: 2vw;
            line-height: 120%;
            color: white;
            padding-left: 0px;
          }
          p {
            font-size: 1.2vw;
            padding-top: 20px;
            padding-left: 0px;
          }

          span {
            font-size: 1.2vw;
            padding-top: 5px;
            padding-left: 0px;
            padding-bottom: 0px;
          }

          .container img {
            border-radius: 16px;
            object-fit: cover;
            width: 20%;
            margin-left: 0px;
          }

          button {
            background-color: #2081e2;
            padding: 0.5vw 2vw;
            border: transparent;
            border-radius: 2vw;
            margin-left: 0px;
            color: white;
          }
        }

        @media screen AND (max-width: 526px) {
          .container {
            width: 75%;
          }

          h2 {
            font-size: 5vw;
            line-height: 120%;
            color: white;
            padding-left: 0px;
          }
          p {
            font-size: 3vw;
            line-height: 130%;
            padding-top: 20px;
            padding-left: 0px;
          }

          span {
            font-size: 2.5vw;
            padding-top: 5px;
            padding-left: 0px;
            padding-bottom: 0px;
          }

          .container img {
            border-radius: 16px;
            object-fit: cover;
            width: 30%;
            margin-left: 0px;
          }

          button {
            background-color: #2081e2;
            padding: 2vw 2vw;
            border: transparent;
            border-radius: 2vw;
            margin-left: 0px;
            color: white;
          }
        }
      `}</style>
    </>
  );
}

export default CollectionDetailedItem;
