import React, { useState } from "react";

type CollectionDetailedItemStatsProps = {
  ownedAsset: string;
  volume: number;
  totalSupply: string;
  ownerNumber: string;
  totalSales: string;
};


/**
 *
 * Stats of the collection shown at the detailed modal.
 *
 * @param {String} ownedAsset - How many asset the user owns from the collection.
 * @param {String} volume - The volume of the collection.
 * @param {String} totalSupply - The total supply of the collection.
 * @param {String} ownerNumber - The owner number of the collection.
 * @param {String} totalSales - The total sales of the collection.
 *
 */


function CollectionDetailedItemStats(props: CollectionDetailedItemStatsProps) {
  const [texts] = useState({
    ownedAsset: props.ownedAsset,
    volume: props.volume,
    totalSupply: props.totalSupply,
    ownerNumber: props.ownerNumber,
    totalSales: props.totalSales,
  });

  return (
    <>
      <div className="details">
        <div className="detail-block">
          <p>{"Volume"}</p>
          <p>{props.volume.toFixed(2)+ " ETH"}</p>
        </div>
        <div className="detail-block">
          <p>{"Owned"}</p>
          <p>{props.ownedAsset}</p>
        </div>
        <div className="detail-block">
          <p>{"Total Supply"}</p>
          <p>{props.totalSupply}</p>
        </div>
        <div className="detail-block">
          <p>{"Number of Owners"}</p>
          <p>{props.ownerNumber}</p>
        </div>
        <div className="detail-block">
          <p>{"Total Sales"}</p>
          <p>{props.totalSales}</p>
        </div>
      </div>
      <style jsx>{`
        .details {
          position: relative;
          box-sizing: border-box;
          display: grid;
          grid-template-columns: repeat(3, 1fr);
          grid-gap: 2vw;
          padding: 20px;
        }

        p {
          font-size: 0.8vw;
          color: white;
        }

        .details .detail-block {
          display: flex;
          flex-direction: column;
          row-gap: 10px;
        }

        @media screen AND (max-width: 1200px) {
          p {
            font-size: 1.2vw;
          }
        }

        @media screen AND (max-width: 950px) {
          p {
            font-size: 1.5vw;
          }
          .details {
            position: relative;
            box-sizing: border-box;
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-gap: 2vw;
            padding-left: 0px;
            padding-top: 10px;
          }
        }

        @media screen AND (max-width: 526px) {
          p {
            font-size: 3.5vw;
          }
          .details {
            position: relative;
            box-sizing: border-box;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 2vw;
          }
        }
      `}</style>
    </>
  );
}

export default CollectionDetailedItemStats;
