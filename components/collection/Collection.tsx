import { useState } from "react";
import CollectionGrid from "./CollectionGrid";

type CollectionProps = {
  collectionData: [];
};

/**
 *
 * Collection page, you can see the collections, volume of collections and amount of the NFT you own from that collection
 *
 * @param {String} title - Title of the component.
 * @param {any[]} collectionData - Collection data.
 *
 */

export default function Collection(props: CollectionProps) {
  const [texts] = useState({
    title: "Your NFTs",
  });

  return (
    <>
      <div className={`intro`}>
        <div className="content">
          <h1>{texts.title}</h1>
        </div>
      </div>
      <div className="grid">
        <CollectionGrid data={props.collectionData} />
      </div>
      <style jsx>{`
        .intro {
          background-color: #000000;
          padding: 2.431vw 3.571vw;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        .intro .content {
          display: flex;
          flex-direction: column;
        }

        .intro .content h1 {
          font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
            Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
          font-style: normal;
          font-weight: 500;
          font-size: clamp(5.0264vw, 5.0264vw, 154.5px);
          line-height: 115%;
          color: color(display-p3 0.549 0.929 0.996);
          margin-bottom: 1.435vw;
          color: white;
          text-align: center;
        }

        .intro hr {
        }

        @media screen AND (min-width: 1921px) {
          .intro .content h1 {
            font-size: 4vw;
            width: 25.807vw;
          }

          .intro .content p {
            font-size: 1.197vw;
            width: 23.177vw;
          }
        }

        @media screen AND (max-width: 526px) {
          .intro {
            padding-left: 4vw;
            padding-right: 4vw;
            flex-direction: column;
          }

          .intro .content h1 {
            font-size: 12vw;
            width: 100%;
            line-height: 120%;
            font-weight: 500;
            text-align: center;
            text-shadow: 0 0 24px #1e4b6b88;
            margin-bottom: 16px;
          }

          .intro .content p {
            font-size: 4vw;
            line-height: 128%;
            width: 100%;
            padding: 0 15px;
            font-weight: 300;
            text-align: center;
          }
        }
      `}</style>
      <style jsx global>{`
        .intro .content .action span strong {
          font-family: Visby, -apple-system, BlinkMacSystemFont, "Segoe UI",
            Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
          font-style: normal;
          font-weight: 400;
          font-size: clamp(1.25661vw, 1.25661vw, 38.63px);
          line-height: 128%;
          color: #fdbab2;
        }

        @media screen and (max-width: 992px) {
          .intro .content .action span strong {
            font-size: 15.6443px;
            line-height: 128%;
          }
        }

        @media screen AND (min-width: 526px) AND (max-width: 992px) {
          .intro .content .action span strong {
            font-size: 20px;
          }
        }

        @media screen AND (min-width: 1921px) {
          .intro .content .action span strong {
            font-size: 1vw;
          }
        }
      `}</style>
    </>
  );
}
