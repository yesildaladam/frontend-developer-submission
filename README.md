## How To Use 

From your command line and clone.

```bash
# Clone this repository
git clone https://gitlab.com/yesildaladam/frontend-developer-submission.git

# Go into the repository
cd frontend-developer-submission

# Install dependencies
yarn

# Start a local dev server
yarn dev
```
